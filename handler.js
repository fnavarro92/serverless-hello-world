'use strict';

module.exports.hello = async event => {
  return {
    statusCode: 200,
    body: JSON.stringify(
      {
        message: "Hello World",
      },
      null,
      2
    ),
  };
};

module.exports.helloName = async event => {

  let message = "Hello World";
  const name = event.pathParameters !== undefined && event.pathParameters.name;
  if (name) {
    message = "Hello " + name;
  }
  return {
    statusCode: 200,
    body: JSON.stringify(
      {
        message: message,
      },
      null,
      2
    ),
  };

}
